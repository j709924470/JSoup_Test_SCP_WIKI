import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class mainpack {

	public static void main(String[] args) {
		try {
			File f = new File("D:\\eclipse\\workplace_n\\JsoupTest\\src\\scp.txt");
			Document doc = Jsoup.parse(f, "UTF-8", "http://scp-wiki-cn.wikidot.com/");/*Jsoup.connect("http://scp-wiki-cn.wikidot.com/")
					.userAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.108 Safari/537.36")
					.timeout(20*1000)
					.get();*/
			Elements es = doc.getElementsByClass("side-block");
			Element ess = es.first();
	        for (Element e : es){
	            for (Element ex : e.children()){
	                if (ex.className().contains("heading")){
	                	ess = e;
	                    print(ex.text());
	                }
	            }
	        }
	        print("heading");
	        List<wpditems> wp = new ArrayList<>();
	        for (Element i : ess.children()){
	            if (i.className().contains("heading")) {
	            	print(i.text());
	            	continue;
	            }
	            for (Element j : i.children()){
	                if (!j.tagName().equals("a")) continue;
	                wpditems wi = new wpditems();
	                wi.name = j.text();
	                print("Name : " + j.text() + " , Url : " + j.attr("abs:href"));
	                wi.url = j.attr("abs:href");
	                wp.add(wi);
	            }
	        }
	        print("Test sideItems");
	        List<WikiItems> lw = sideitems(ess);
	        plist(lw);
	        print("Test pageItems");
	        List<WikiItems> lp = pageitems(Jsoup.parse(new File("D:\\eclipse\\workplace_n\\JsoupTest\\src\\scp-page.txt"),"UTF-8","http://scp-wiki-cn.wikidot.com/"));
	        plist(lp);
	        print("Test PageContent");
	        File fl = new File("D:\\eclipse\\workplace_n\\JsoupTest\\src\\result.html");
	        if (fl.exists()) fl.delete();
	        fl.createNewFile();
	        FileWriter fw = new FileWriter(fl);
	        String result = parseContent(Jsoup.parse(new File("D:\\eclipse\\workplace_n\\JsoupTest\\src\\scp-content.htm"), "UTF-8", "http://scp-wiki-cn.wikidot.com/scp-002"));
	        fw.append(result);
	        fw.close();
		} catch (Exception e) {
			print("What The ..?");
			e.printStackTrace();
		}
	}
	
	private static Elements getStylesheets(Document doc){
        List<Element> le = new ArrayList<>();
        Elements ell = doc.getElementsByTag("link");
        Elements els = doc.getElementsByTag("style");
        for (Element el : ell){
        	print(el.tag());
            le.add(el);
        }
        for (Element el : els){
        	print(el.tag());
            le.add(el);
        }
        return new Elements(le);
    }
	
	public static String parseContent(Document doc){
        Element pgct = getPageContent(doc);
        pgct.getElementsByClass("footer-wikiwalk-nav").remove();
        StringBuilder sb = new StringBuilder();
        sb.append(pgct.html());
        Elements els = getStylesheets(doc);
        for (Element el : els){
        	sb.append("\r\n<"+el.tag()+" "+el.attributes()+">");
            sb.append(el.html());
            sb.append("\r\n</"+el.tag()+">");
        }
        return sb.toString();
    }
	
	private static Element getPageContent(Document doc){
        int f = 0;
        Element emt = doc.getElementById("main-content");
        Element e2 = null;
        for (Element e1 : emt.children()){
            if(e1.id().equals("page-title")){
                f++;
            }
            if(e1.id().equals("page-content")){
                e2 = e1;
                f++;
            }
            if(e2 == e1 && f == 2) break;
        }
        return e2;
    }
	
	public static List<WikiItems> pageitems(Document doc){
		Element em = getPageContent(doc);
		List<WikiItems> lwi = new ArrayList<>();
		String itx = null;
        WikiItems wi = new WikiItems();
        List<WikiSubItems> sar = new ArrayList<>();
        boolean f = false;
        print(em.children().size());
        while (em.children().size() <= 2) {
        	em = em.children().first();
        }
        //print(em.children().get(0));
		for (Element e : em.children()) {
			if (e.id().contains("to") && e.tagName() == "h1" && itx != e.text() && !f) {
				f = true;
				wi.wsi = sar;
				lwi.add(wi);
				wi = new WikiItems();
				itx = e.text();
				wi.name = e.text();
				sar = new ArrayList<>();
				continue;
			}
			if(f && e.tagName() == "ul") {
				f = false;
				for (Element i : e.children()) {
					WikiSubItems wsi = new WikiSubItems();
					wsi.name = i.text();
					wsi.url = i.getElementsByTag("a").attr("abs:href");
					sar.add(wsi);
				}
			}
		}
		return lwi;
	}

	public static List<WikiItems> sideitems(Element elm){
        Element emt = elm;
        List<WikiItems> arr = new ArrayList<>();
        String itx = null;
        WikiItems wi = new WikiItems();
        List<WikiSubItems> sar = new ArrayList<>();
        for (Element i : emt.children()){
            if (i.className().contains("heading")) {
            	if (itx != i.text()) {
            		wi.wsi = sar;
            		arr.add(wi);
            		itx = i.text();
            		wi = new WikiItems();
            		wi.name = i.text();
            		sar = new ArrayList<>();
            	}
                continue;
            }else {
                for (Element j : i.children()) {
                    if (!j.tagName().equals("a")) continue;
                    WikiSubItems wsi = new WikiSubItems();
                    wsi.name = j.text();
                    wsi.url = j.attr("abs:href");
                    sar.add(wsi);
                }
            }
        }
        return arr;
    }
	
	private static void print(Object arg) {
		System.out.print(arg);
		System.out.print("\r\n");
	}
	private static void print(Object[] args) {
		int ix = 1;
		print("Array:");
		for (Object i : args) {
			print("\t" + (ix) + " : " + i);
			ix++;
		}
	}
	
	private static void plist(List<WikiItems> lwi) {
		if (lwi == null) {
			print("NULL");
			return;
		}
		for (WikiItems wi : lwi) {
			print("Name: "+wi.name);
			for (WikiSubItems wsi : wi.wsi) {
				print("\tSub Name: " + wsi.name);
				print("\tSub URL: "+wsi.url);
			}
		}
	}
	
	private static void pels(Elements els) {
		for (Element i : els) {
			print(i.html());
		}
	}
	
	private static void printl(List<WikiSubItems> lwi) {
		for (WikiSubItems wi : lwi) {
			print("Sub Name: "+wi.name);
			print("Sub URL: "+wi.url);
		}
	}
}

class WikiItems {
	public String name;
	public List<WikiSubItems> wsi;
}

class WikiSubItems {
	public String name;
	public String url;
}

class wpditems {
	public String name;
	public String url;
}
